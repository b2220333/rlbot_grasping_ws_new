# Embodied Cognition Project

## Instructions to run:

#### 1. Go to the home workspace folder and run `catkin_make` to make and build the workspace

#### 2. Source your workspace in your bash file for convenience: `echo 'source ~/rlbot_grasping_ws/devel/setup.bash' > ~/.bashrc  && source ~/.bashrc` 

#### 3. Run this sequence after making the packages for grasping the cube: 
* Launches gazebo with table + cube environment, spawns robot near table ; Launches RViz; Launches move_group node, which is responsible for inverse kinematics and joint actuation of arm; Launches basic_grasping_perception which is responsible for perception of objects:

    `roslaunch rlbot_gazebo demo.launch` 

* Skip this step!!! Uses external controllers to change joint1, joint2 positions so as to not obstruct the view of kinect (point cloud can be seen in rviz):

    `roslaunch rlbot_gazebo put_arm_aside.launch`

* Python script to send goals to navigation stack as well as move_group;Navigation goal causes the robot to go closer to the table (Manually input coordinates); Loop to perceive object:

    `rosrun rlbot_gazebo demo.py`
    'rosrun rlbot_gazebo smart_grasper.py'

## Package Explanations: 

**rlbot_description** :
* Using rlbot.xacro. Robot described in a modular fashion with the base, arm and kinect imported from their respective folders. 
* Not using the urdf folder. It was used for checking if it made a difference to describe the robot in a single urdf code instead. There was no difference found. 
* Contains rlbot.launch.xml which contains details of which nodes to be launched with specific parameters with respect to the robot. For eg. tf transforms, arm controllers etc. This file is launched from playground.launch

**rlbot_gazebo**
* config: Contains YAML files for gazebo controllers (Trajectory controllers)
* launch: Contains necessary launch files for spawning the robot in the virtual environment
* maps: Contains maps of table environment, as well as fetch environment (http://docs.fetchrobotics.com/gazebo.html)

**rlbot_maps**: 
* Contains maps of different environments. Can be ignored for now. 

**rlbot_control**: 
* rlbot_controller.launch launches controller_manager node which acts as controllers for the arm. 
* These controllers are used to manually move the arm away in put_arm_aside.launch. 
* It is not clear if these controllers override gazebo controllers or if they work in tandem. 

**rlbot_moveit_config** :
* The package solely defined for grasping purpose. 
* move_group is launched through demo.launch. 
* The configuration files are created through MoveIt Setup Assistant	(http://docs.ros.org/hydro/api/moveit_setup_assistant/html/doc/tutorial.html)

**rlbot_navigation**:
* Contains navigation stack info. 
* rlbot_nav.launch which has the navigation stack (AMCL and move_base) is launched by demo.launch



#### This style of packaging was mimiced from Fetch Robotics (http://docs.fetchrobotics.com/gazebo.html) which uses a similar setting to grasp a cube and place it on a different table. 

## Problems Encountered:

* Controllers: There is some confusion with respect to gazebo plugins used to interface the robot with control packages. Fetch Robotics uses a plugin to create an action server to control joints of the robot but the same does not work for our robot. Check rlbot.xacro in rlbot_description. Defining our own controller using controller_manager also doesn't seem to work
* prepare_simulated_robot.py is a python script inside scripts in rlbot_gazebo. It is used to set the robot up by tucking the arm. It does this by sending a goal to the action client which interacts with the action server created by the plugin mentioned above to execute the action. But upon execution, some transforms become faulty and the robot flies to (0,0,0) pose. We are unable to solve this problem. This is the reason put_arm_aside.launch was created to publish messages directly on topics to move the arm. This is only a temporary fix as it is inconvenient to move the arm this way every time. 
* demo.py in scripts in rlbot_gazebo does navigation properly but throws errors in perception and grasping. It could be because of faulty controllers but we're unable to pinpoint the exact reason for the errors.
